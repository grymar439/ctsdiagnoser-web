import firebase from "firebase/compat/app";
import "firebase/compat/storage";

import {
  getAuth, signInWithEmailAndPassword, signOut
} from "firebase/auth";
import {
  getFirestore
} from "firebase/firestore";
const firebaseConfig = {
  apiKey: "AIzaSyDde8ugB7zn-92KPWefB4slfRbqIYYYRwg",
  authDomain: "carpaltunneldiagnoser.firebaseapp.com",
  projectId: "carpaltunneldiagnoser",
  storageBucket: "carpaltunneldiagnoser.appspot.com",
  messagingSenderId: "190424188285",
  appId: "1:190424188285:web:3d8e11cb791f2d6f1ff814",
  measurementId: "G-4FW0HL1PYZ",
};

const app = firebase.initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);

const logInWithEmailAndPassword = async (email, password) => {
  try {
    console.log(email + "@diagnoser.com");
    await signInWithEmailAndPassword(auth, email + "@diagnoser.com", password);
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

const logout = () => {
  signOut(auth);
};

const myStorage = firebase.storage().ref();

export { auth, db, logInWithEmailAndPassword, logout, myStorage };

