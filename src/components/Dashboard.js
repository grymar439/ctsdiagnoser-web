import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { getDownloadURL, getMetadata, listAll } from "firebase/storage";
import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import "../styles/Dashboard.css";
import { auth, logout, myStorage } from "../utility/firebase";
import AnswersList from "./AnswersList.tsx";
import CheckboxList from "./FilesList.tsx";
import LinePlot from "./LinePlot.tsx";

function Dashboard() {
  const [user, loading, error] = useAuthState(auth);
  const navigate = useNavigate();
  const [filesList, setFilesList] = useState([]);
  const [emgList, setEmgList] = useState([]);
  const [answersList, setAnswersList] = useState([]);
  const [checkedList, setCheckedList] = useState([]);
  const [isAnswersShown, setIsAnswersShown] = useState(true);

  const storage = myStorage;
  const fetchAllFiles = async () => {
    var promisesList = [];
    var tempDataList = [];
    const listRef = storage.child(user.email.split("@")[0] + "/");
    listAll(listRef)
      .then((res) => {
        res.items.forEach((itemRef) => {
          const promise1 = getDownloadURL(itemRef);
          const promise2 = getMetadata(itemRef);

          const allPromises = Promise.all([promise1, promise2]);
          promisesList.push(allPromises);
          allPromises.then((itemRef) => {
            tempDataList.push({ name: itemRef[1].name, url: itemRef[0] });
          });
        });
        Promise.all(promisesList).then((elem) => {
          console.log("Promise list" + elem.name);
          setFilesList(tempDataList);
        });
        console.log(filesList);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (loading) return;
    if (!user) return navigate("/");
    fetchAllFiles();
  }, [user, loading]);

  useEffect(() => {
    const tempEMGList = [];
    const tempAnswersList = [];

    filesList.forEach((element) => {
      if (element.name.includes("answers")) {
        tempAnswersList.push(element);
      } else {
        tempEMGList.push(element);
      }
      setAnswersList(tempAnswersList);
      setEmgList(tempEMGList);
    });
    console.log(checkedList);
    console.log(answersList);
    // setCheckedList(Array.from({ length: emgList.length }, (i) => (i = false)));
  }, [filesList]);

  const handleConfirm = (values) => {
    setCheckedList(values);
    console.log(values);
  };

  return (
    <div
      className="center"
      style={{
        width: window.innerWidth - 20,
        height: window.innerHeight,
      }}
    >
      <Stack
        direction="row"
        spacing={1}
        sx={{
          boxShadow: 12,
          margin: 12,
          padding: 4,
        }}
      >
        <div></div>
        <Stack direction="column" spacing={2}>
          Jesteś zalogowany jako:
          <Typography style={{ align: "right" }}>
            {user?.email.split("@")[0]}
          </Typography>
          <Button
            variant="contained"
            color="secondary"
            style={{ maxWidth: "140px", height: "32px" }}
            onClick={logout}
          >
            Wyloguj
          </Button>
          <Stack direction="column" spacing={2}>
            <Stack direction="row" spacing={1}>
              <Button
                variant={isAnswersShown ? "contained" : "outlined"}
                onClick={() => setIsAnswersShown(true)}
              >
                Odpowiedzi
              </Button>
              <Button
                variant={isAnswersShown ? "outlined" : "contained"}
                onClick={() => setIsAnswersShown(false)}
              >
                Wyniki pomiarów EMG
              </Button>
            </Stack>
          </Stack>
          <div>
            {emgList && !isAnswersShown && (
              <Stack direction="row" spacing={2}>
                <CheckboxList filesList={emgList} onConfirm={handleConfirm} />
                {checkedList && <LinePlot selectedItems={checkedList} />}
              </Stack>
            )}
            {answersList && isAnswersShown && (
              <AnswersList answersList={answersList}></AnswersList>
            )}
          </div>
        </Stack>
        <div></div>
      </Stack>
    </div>
  );
}

export default Dashboard;
