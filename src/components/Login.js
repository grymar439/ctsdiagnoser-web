import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { auth, logInWithEmailAndPassword } from "../utility/firebase.js";
import { useAuthState } from "react-firebase-hooks/auth";
import "../styles/Login.css";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Logo from "../assets/logo.png";
import TextField from "@mui/material/TextField";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, loading, error] = useAuthState(auth);
  const navigate = useNavigate();
  const [isAdminLogin, setIsAdminLogin] = useState(false);

  useEffect(() => {
    if (loading) {
      // maybe trigger a loading screen
      return;
    }
    if (user) navigate("/dashboard");
  }, [user, loading]);

  return (
    <div className="login">
      <Stack
        direction="column"
        spacing={1}
        sx={{
          boxShadow: 12,
          margin: 12,
          padding: 4,
        }}
      >
        <img src={Logo} style={{ width: 400, height: 400 }} alt="Logo" />
        <Typography
          sx={{ mt: 4, mb: 2 }}
          fontWeight="bold"
          variant="h4"
          component="div"
        >
          Witamy w aplikacji
        </Typography>
    
        <Typography
          sx={{ mt: 4, mb: 2 }}
          fontWeight="bold"
          variant="h6"
          component="div"
        >
          Wprowadź swoje dane logowania
        
        </Typography>
        <TextField
          id="login_textField"
          label="Login"
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          id="password_textField"
          label="Hasło"
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Stack direction="row" spacing={4}>
          <Button
            variant="contained"
            color={isAdminLogin ? "secondary" : "primary"}
            onClick={() => logInWithEmailAndPassword(email, password)}
          >
            Zaloguj
          </Button>
        </Stack>
      </Stack>
    </div>
  );
}

export default Login;
