import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Papa from "papaparse";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import React, { useState, useEffect } from "react";
import { Divider } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import DownloadForOfflineIcon from "@mui/icons-material/DownloadForOffline";

export default function AnswersList({ answersList }) {
  const [answers, setAnswers] = useState<string[]>([]);
  const handleClick = (value) => () => {
    var tempTextList: any[] = [];
    Papa.parse(value.url, {
      header: true,
      delimiter: ",",
      mode: "cors",
      download: true,
      complete: (results) => {
        results.data.forEach((element) => {
          console.log(element[""]);
          tempTextList.push(element[""]);
        });
        console.log(tempTextList);
        setAnswers(tempTextList);
      },
    });
  };

  const openInNewTab = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  if (answersList) {
    return (
      <div>
        <Stack direction="row" spacing={2}>
          <List
            sx={{
              width: "100%",
              maxWidth: 360,
              height: 600,
              overflow: "auto",
              bgcolor: "background.paper",
            }}
          >
            {answersList.map((value) => {
              const labelId = `checkbox-list-label-${value.name}`;

              return (
                <ListItem
                  style={{ display: "flex", gap: "8px" }}
                  key={value.name}
                  secondaryAction={
                    <IconButton
                      edge="end"
                      aria-label="comments"
                      onClick={() => openInNewTab(value.url)}
                    >
                      <DownloadForOfflineIcon />
                    </IconButton>
                  }
                  disablePadding
                >
                  <ListItemButton
                    role={undefined}
                    onClick={handleClick(value)}
                    dense
                  >
                    <ListItemText
                      style={{ wordWrap: "break-word" }}
                      id={labelId}
                      primary={value.name}
                    />
                  </ListItemButton>
                </ListItem>
              );
            })}
          </List>
          <div>
            <Typography
              sx={{ mt: 0, mb: 0 }}
              fontWeight="regular"
              variant="h6"
              component="div"
            >
              Odpowiedzi
            </Typography>
            <List
              sx={{
                width: "100%",
                maxWidth: 360,
                maxHeight: 560,
                overflow: "auto",
                bgcolor: "background.paper",
              }}
            >
              {answers.map((element) => {
                return (
                  <ListItem
                    style={{ display: "flex", gap: "2px" }}
                    key={element}
                    disablePadding
                  >
                    <ListItemText
                      style={{ wordWrap: "break-word" }}
                      id={element}
                      primary={element}
                    />
                  </ListItem>
                );
              })}
            </List>
          </div>
          <Divider />
          <div>
            <Typography
              sx={{ mt: 0, mb: 0 }}
              fontWeight="regular"
              variant="h6"
              component="div"
            >
              Pytania w Bostońskim Kwestionariuszu Zespołu Cieśni Nadgarstka
            </Typography>
            <Typography
              sx={{ mt: 1, mb: 1 }}
              fontWeight="regular"
              variant="subtitle2"
              component="div"
            >
              Skala Nasilenia Objawów (SSS)
            </Typography>
            <Typography
              sx={{ mt: 4, mb: 2 }}
              fontWeight="regular"
              style={{ whiteSpace: "pre-line" }}
              variant="caption"
              component="div"
            >
              1. Jak dokuczliwy jest ból dłoni lub nadgarstka u Pana/Pani w
              nocy?
              {"\n"}
              2. Jak często ból dłoni lub nadgarstka budził Pana/Panią w ciągu
              typowej nocy podczas ostatnich dwóch tygodni?
              {"\n"}
              3. Czy w ciągu dnia zazwyczaj odczuwa Pan/Pni ból w okolicy
              nadgarstka?
              {"\n"}
              4. Jak często w ciągu dnia występuje u Pana/Pani ból w okolicy
              nadgarstka?
              {"\n"}
              5. Jak długo w Pana/Pani przypadku trwa standardowy epizod bólu w
              okolicy nadgarstka w ciągu dnia?
              {"\n"}
              6. Czy występuje u Pana/Pani utrata czucia w dłoni?
              {"\n"}
              7. Czy występuje u Pana/Pani osłabienie dłoni lub nadgarstka?
              {"\n"}
              8. Czy występuje u Pana/Pani uczucie mrowienia w dłoni?
              {"\n"}
              9. Jak bardzo nasilona jest u Pana/Pni utrata czucia lub mrowienie
              podczas nocy?
              {"\n"}
              10. Jak często drętwienie lub uczucie mrowienia lub utrata czucia
              w dłoni budziły Pana/Panią podczas typowej nocy w przeciągu
              ostatnich 2 tygodni?
              {"\n"}
              11. Czy ma Pan/Pani problem z chwytaniem i używaniem niewielkich
              przedmiotów, takich jak klucze czy długopis?
            </Typography>
            <Typography
              sx={{ mt: 4, mb: 2 }}
              fontWeight="regular"
              variant="subtitle2"
              style={{ whiteSpace: "pre-line" }}
              component="div"
            >
              Skala Stanu Funkcjonalnego (FSS)
              {"\n"}
              (Określenie trudności w wykonywaniu poniższych codziennych
              czynności w skali 1-5)
            </Typography>
            <Typography
              sx={{ mt: 4, mb: 2 }}
              fontWeight="regular"
              variant="caption"
              style={{ whiteSpace: "pre-line" }}
              component="div"
            >
              12. Pisanie
              {"\n"}
              13. Zapinanie guzików w ubraniach
              {"\n"}
              14. Trzymanie książki podczas czytania
              {"\n"}
              15. Chwytanie i używanie telefonu
              {"\n"}
              16. Otwieranie słoików
              {"\n"}
              17. Prace domowe
              {"\n"}
              18. Noszenie toreb z zakupami
              {"\n"}
              19. Mycie się i ubieranie
            </Typography>
          </div>
        </Stack>
      </div>
    );
  }
}
