import React, { useState, useEffect } from "react";
import Papa from "papaparse";
import Box from "@mui/material/Typography";
import {
  XYPlot,
  XAxis,
  YAxis,
  ChartLabel,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries,
  DiscreteColorLegend,
  ContinuousSizeLegend,
} from "react-vis";
import Stack from "@mui/material/Stack";
import { transformLabel } from "@antv/g2plot/lib/utils";
import { Line } from "@ant-design/plots";
import { popperUnstyledClasses } from "@mui/base";
import { sizeWidth } from "@mui/system";

export default function LinePlot({ selectedItems }) {
  const [data, setData] = useState<
    {
      dataSet: {
        x: number;
        y: number;
      }[];
    }[]
  >();
  const [names, setNames] = useState<string[]>();
  const Line = LineSeries;
  // const url =
  //   "https://firebasestorage.googleapis.com/v0/b/carpaltunneldiagnoser.appspot.com/o/mg%2Fmama_Po%20zabiegu_right_11-11-22_18-03.csv?alt=media&token=72298eb5-2897-46fe-b8e9-a597fcaefecd";
  useEffect(() => {
    fetch();
    console.log(selectedItems);
  }, [selectedItems]);

  const fetch = () => {
    const tempNames: string[] = [];
    var promisesList: Promise<any>[] = [];
    selectedItems.forEach((element) => {
      const dataPoints: {
        x: number;
        y: number;
      }[] = [];

      promisesList.push(
        new Promise((resolve, reject) => {
          Papa.parse(element.url, {
            header: true,
            delimiter: ",",
            mode: "cors",
            download: true,
            complete: (results) => {
              for (let row of results.data) {
                try {
                  if (row["EMG_CH2_24BIT"].toString() !== "NaN") {
                    dataPoints.push({
                      x:
                        parseFloat(row["Timestamp"]) -
                        parseFloat(results.data[0]["Timestamp"]),
                      y: parseFloat(row["EMG_CH2_24BIT"]),
                    });
                  }
                } catch {
                  continue;
                }
              }
              resolve({ dataSet: dataPoints });
              tempNames.push(element.name);
              setNames(tempNames);
            },
          });
        })
      );
    });
    const listAllTemp2: {
      dataSet: {
        x: number;
        y: number;
      }[];
    }[] = [];
    Promise.all(promisesList).then((listItems) => {
      console.log("List items:", listItems);
      listItems.forEach((elem) => {
        listAllTemp2.push(elem);
      });
      setData(listAllTemp2);
    });
  };

  if (data && names && data.length === selectedItems.length) {
    return (
      <div>
        <XYPlot width={window.innerWidth - 700} height={500}>
          <HorizontalGridLines />
          <VerticalGridLines />
          <XAxis title="Czas [s]" />
          <YAxis title="EMG [mV]" />
          <ChartLabel
            className="alt-x-label"
            includeMargin={false}
            xPercent={0.5}
            yPercent={0.1}
            style={{
              textAnchor: "end",
            }}
          />

          <DiscreteColorLegend
            height={800}
            style={{ left: "950px", top: "1000px" }}
            orientation="vertical"
            items={names}
          />

          {data.map((element) => {
            return (
              <Line
                style={{ fill: "none" }}
                className="second-series"
                strokeWidth={2}
                data={element.dataSet}
              />
            );
          })}
        </XYPlot>
        <Box sx={{ m: names.length * 2.5 + 2 }}></Box>
      </div>
    );
  }
}
