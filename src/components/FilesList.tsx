import * as React from "react";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import FolderIcon from "@mui/icons-material/Folder";
import List from "@mui/material/List";
import ListItemIcon from "@mui/material/ListItemIcon";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import DownloadForOfflineIcon from '@mui/icons-material/DownloadForOffline';

export default function FilesList({ filesList, onConfirm }) {
  const [checked, setChecked] = React.useState<string[]>([]);

  const handleToggle = (value) => () => {
    const temporaryCheckedList = [...checked];

    if (temporaryCheckedList.includes(value)) {
      temporaryCheckedList.splice(temporaryCheckedList.indexOf(value), 1);
    } else {
      temporaryCheckedList.push(value);
    }
    setChecked(temporaryCheckedList);
  };

  const confirm = () => {
    console.log("Checked", checked);
    onConfirm(checked);
  };

  const openInNewTab = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  return (
    <div>
      <List
        sx={{
          width: "100%",
          maxWidth: 360,
          maxHeight: 500,
          overflow: "auto",
          bgcolor: "background.paper",
        }}
      >
        {filesList.map((value) => {
          const labelId = `checkbox-list-label-${value.name}`;

          return (
            <ListItem
              style={{ display: "flex", gap: "8px" }}
              key={value.name}
              secondaryAction={
                <IconButton
                  edge="end"
                  aria-label="comments"
                  onClick={() => openInNewTab(value.url)}
                >
                  <DownloadForOfflineIcon />
                </IconButton>
              }
              disablePadding
            >
              <ListItemButton
                role={undefined}
                onClick={handleToggle(value)}
                dense
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={checked.includes(value)}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                <ListItemText
                  style={{ wordWrap: "break-word" }}
                  id={labelId}
                  primary={value.name}
                />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <Button variant="contained" color="success" onClick={confirm}>
        Zatwierdź
      </Button>
    </div>
  );
}
